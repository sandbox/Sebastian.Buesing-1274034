/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
  Drupal.behaviors.drupli_search_refine = {
    attach: function (context) {
      Drupal.behaviors.drupli_search_refine.page = 0;
      // adjust view filter form handling
      Drupal.behaviors.drupli_search_refine.overrideViewsHandler();
      
      // @TODO: implement AND / OR feature
      Drupal.behaviors.drupli_search_refine.overrideFulltextField();
      Drupal.behaviors.drupli_search_refine.submitSearch();
      Drupal.behaviors.drupli_search_refine.solrDebug(Drupal.settings.drupli_search.solr_debug);
    },
    solrDebug: function(data) {
      if (data) {
        $parent = $('#edit-search-api-views-fulltext').parent();
        $parent.append('<div id="solr_debug">' + data +'</div>');
      }
    },
    overrideFulltextField: function () {
      // update form on page load
      $filter = Drupal.settings.drupli_search.filter.search_api_views_fulltext;
      // Don't be the exception value into the form, just make it blank
      if ($filter.current_value == $filter.exception) {
        $('#edit-search-api-views-fulltext').attr('value', '');
      } else {
        // we have a value, lets analyze it
        $form_value = Drupal.behaviors.drupli_search_refine.parseArg($filter, $filter.current_value, true);
        // highlight
        Drupal.behaviors.drupli_search_refine.higlightTerms($filter.current_value);
        // and override form input value
        $('#edit-search-api-views-fulltext').attr('value', $form_value);
      }
    },
    higlightTerms: function (values) {
      if (!$.isArray(values)) {
        //$('#content').highlight(values.replace(/\"/g, ''));
      } else {
        for (i = 1; i < values.length; i++) {
          Drupal.behaviors.drupli_search_refine.higlightTerms(values[i]);
        }
      }
    },
    submitSearch: function () {
      var base = location.protocol +"//"+ location.host + Drupal.settings.basePath;
      var start = Drupal.settings.basePath.length;
      var end = location.pathname.indexOf(Drupal.settings.drupli_search.path);
      base += location.pathname.substring(start, end);
      //'search-content/';
      $url = '';
      //
      for(var key in Drupal.settings.drupli_search.filter) {
        // we can just use
        $filter = Drupal.settings.drupli_search.filter[key];
        // check if we have fulltext filter - otherwise use exception value
        if(key == 'search_api_views_fulltext') {
          if($filter.seo_optimized_active) {
            $url = $url + $filter.seo_optimized_key + $filter.seo_optimized_key_value_seperator;
          }
          // check the user entry
          if($('#edit-search-api-views-fulltext').attr('value') == '') {
            $url = $url + $filter.exception + '/';
          } else {
            var val = $('#edit-search-api-views-fulltext').attr('value');
            $url = $url + Drupal.behaviors.drupli_search.parseFulltextField(val) + '/';
          }
        } else {
          // only add keyword if active and value is not the exception value
          if($filter.seo_optimized_active && $.isArray($filter.current_value)) {
            $url = $url + $filter.seo_optimized_key + $filter.seo_optimized_key_value_seperator;
          }
          if($.isArray($filter.current_value)) {
            $url = $url + Drupal.behaviors.drupli_search_refine.parseArg($filter, $filter.current_value) + '/';
          }
        }
      }
      if ('replaceState' in history) {
        history.replaceState(null, document.title, base + Drupal.settings.drupli_search.path + '/' + $url);
      }
      $.post(base + "?q=search/load_view/search-content", {url: $url, page: Drupal.settings.drupli_search.page}, Drupal.behaviors.drupli_search_refine.searchLoaded, 'json');
      //$('#content').removeHighlight();
    },
    parseArg: function (filter, values, fulltext_mode) {
      // if we have only one value we can just return that one
      if (values.length == 2) {
        if (fulltext_mode && values[1].indexOf(' ') != -1) {
          values[1] = '"' +  values[1] + '"';
        }
        return values[1];
      }
      // otherwise we have to build the term together correctly
      // first element is the conjunction
      var conjunction = values[0];
      // we need this variable in case cunjunction is OR and we found a
      // subconjunction of type AND - in that we have to use the S(uper)OR
      var and_found = false;
      // collect terms inside array
      var terms = new Array();
      // we loop through values
      for (i = 1; i < values.length; i++) {
        // check for subfilter
        if ($.isArray(values[i])) {
          // check for AND
          if (values[i][0] == 'AND') {
            and_found = true;
          }
          // somehow the subcall messed up the 'i' variable
          $before = i;
          terms.push(Drupal.behaviors.drupli_search_refine.parseArg(filter, values[i], fulltext_mode));
          i = $before;
        } else {
          // in fulltext mode values with spaces are by definition quotes
          if (fulltext_mode && values[i].indexOf(' ') != -1) {
            values[i] = '"' +  values[i] + '"';
          }
          terms.push(values[i]);
        }
      }
      if(fulltext_mode) {
        // check if we have to use the S(uper)OR
        if (conjunction == 'OR') {
          if(and_found) {
            conjunction = ' OR ';
          } else {
            conjunction = ',';
          }
        } else {
          conjunction = ' ';
        }
      } else {
        // check if we have to use the S(uper)OR
        if (conjunction == 'OR') {
          if(and_found) {
            conjunction = filter.seo_optimized_sor_value_seperator;
          } else {
            conjunction = filter.seo_optimized_or_value_seperator;
          }
        } else {
          conjunction = filter.seo_optimized_and_value_seperator;
        }
      }
      var arg = terms[0]
      // append the rest
      for (i = 1; i < terms.length; i++) {
        arg += conjunction + terms[i];
      }
      // and return the well-formed string
      return arg;
    },
    searchLoaded: function (data) {
      //
      $('.content', $('#block-system-main')).html(data['output']);
      Drupal.behaviors.drupli_search_refine.solrDebug(data['solr_debug']);
      // update fulltext form field
      Drupal.settings.drupli_search.filter.search_api_views_fulltext.current_value = data['fulltext_current'];
      Drupal.behaviors.drupli_search_refine.overrideFulltextField();
      // and override view handlers again
      Drupal.behaviors.drupli_search_refine.overrideViewsHandler();
      // adjust refine search block
      var output = '<div class="filter_box">';
      for (var key in Drupal.settings.drupli_search.block) {
        // 
        if(data['filter'][key]) {
          output += '<div class="filter_category"><label>' + Drupal.settings.drupli_search.block[key].label + '</label>';
          output += '<ul class="filter_items">';
          // loop through hits
          for(i = 0; i < data['filter'][key].length; i++) {
            output += '<li class="filter_item">';
            output += '<a name="' + data['filter'][key][i]['name'] + '">';
            output += data['filter'][key][i]['name'] +  ' (' + data['filter'][key][i]['count'] + ')</a></li>';
          }
          output += '</ul>';
        }
        output += '<input type="hidden" id="drupli_search-refine_field-' + key + '" name="' + key + '" value="' + key + '" /></div>';
      }
      $('div.content', $('#block-drupli_search-drupli_search_refine')).html(output);
      Drupal.behaviors.drupli_search_refine.enableFilterHandler();
    },
    paging: function (href) {
      var index = href.indexOf('&page=');
      if(index) {
        index += 6;
        var page = href.substr(index, href.length - index);
        Drupal.settings.drupli_search.page = page;
        Drupal.behaviors.drupli_search_refine.submitSearch();
        $.scrollTo({top:0},{duration:1000, axis:'y'});
      }
    },
    overrideViewsHandler: function () {
      $('#edit-submit-search-content').bind('click', function( event ) {
        Drupal.settings.drupli_search.page = 0;
        Drupal.behaviors.drupli_search_refine.submitSearch();
        return false;
      });
      $('#edit-reset').remove();
      // adjust paging too
      $('ul.pager > li > a',$('#content')).each(function (item) {
        $link = $(this);
        $link.bind('click', function( event ) {
            Drupal.behaviors.drupli_search_refine.paging($(this).attr('href'));
          return false;
        });
      });
    },
    enableFilterHandler: function () {
      $('div.filter_category > ul > li > a',$('#block-drupli_search-drupli_search_refine')).each(function (item) {
        $link = $(this);
        $key = $('input', $link.parent().parent().parent());
        $link.bind('click', function( event ) {
            $(this).toggleClass('selected');
            Drupal.behaviors.drupli_search_refine.refine_search($('input', $(this).parent().parent().parent()).attr('name'), $(this).attr('name'), 'AND');
          return false;
        });
      });
      for(var key in Drupal.settings.drupli_search.filter) {
        if(key != 'search_api_views_fulltext') {
          var terms = Drupal.settings.drupli_search.filter[key].current_value;
          if($.isArray(terms)) {
            for(var i in terms) {
              $(('ul > li > a'), $('#drupli_search-refine_field-' + key).parent()).each(function(item) {
                $link = $(this);
                if($link.attr('name') == terms[i]) {
                  $link.toggleClass('selected');
                }
              });
            }
          }
        }
      }
    },
    refine_search: function(filter_key, value, conjunction) {
      $filter = Drupal.settings.drupli_search.filter[filter_key];
      // if we have the exception value, than replace it with an array 
      // containing the current value
      if($filter.current_value == $filter.exception) {
        // filter does not have a value yet and can be created from scratch
        $filter.current_value = new Array(conjunction, value);
      } else {
        // otherwise we have to either remove it or insert it correctly
        var result = Drupal.behaviors.drupli_search_refine.refine_search_remove($filter, $filter.current_value, value, conjunction);
        var exists = result[0];
        // grab the new (or same) current value
        $filter.current_value = result[1];
        // check if we didn't create an empty filter
        if (exists && result[1].length == 1) {
          // well than reset the filter
          $filter.current_value = $filter.exception;
        }
        // add the value if it did not exist yet.
        if (!exists) {
          // simplest case first - same cunjunction means we can just
          // append the new value
          if ($filter.current_value[0] == conjunction) {
            $filter.current_value.push(value);
          } else {
            // otherwise we have two situation
            // 1. AND -> OR
            // 2. OR -> AND
            // which we must handle differently
            if ($filter.current_value[0] == 'AND') {
              // 1. case
              $filter.current_value = Drupal.behaviors.drupli_search_refine.refine_search_add($filter.current_value, 'OR', value);
            } else {
              // 2. case
              $filter.current_value = Drupal.behaviors.drupli_search_refine.refine_search_add($filter.current_value, 'AND', value);
            }
          }
        }
      }
      Drupal.settings.drupli_search.filter[filter_key].current_value = $filter.current_value;
      Drupal.settings.drupli_search.page = 0;
      Drupal.behaviors.drupli_search_refine.submitSearch();
    },
    refine_search_remove: function($filter, current_value, value, conjunction) {
      // set found flag to false
      var exists = false;
      var temp = new Array(current_value[0]);
      // first we assume it exists and try to remove it
      for (i = 1; i < current_value.length; i++) {
        // for subfilters we use a helper function
        if ($.isArray(current_value[i])) {
          var result = Drupal.behaviors.drupli_search_refine.refine_search_remove($filter, current_value[i], value, conjunction);
          // push the value part
          temp.push(result[1]);
          // and set the exists flag
          exists = result[0];
        } else {
          // compare against our value - has to match conjunction too
          if(current_value[i] == value && current_value[0] == conjunction) {
            exists = true;
          } else {
            temp.push(current_value[i]);
          }
        }
      }
      current_value = temp;
      // check if we didn't create an empty term
      /*
      if (temp.length == 2) {
        // in that case reset filter
        current_value = temp[1];
      }*/
      return new Array(exists, current_value);
    },
    refine_search_add: function (current_value, new_conjunction, value) {
      if (current_value.length == 2) {
        // only one value - the old conjunction is turned into the
        // new conjunction with both values inside
        current_value[0] == new_conjunction;
        current_value.push(value);
      } else {
        // multiple values - we have to check if there is a subfilter
        // inside the values
        var sub_filter_found = false;
        for (var k in current_value) {
          if ($.isArray(current_value[k])) {
            sub_filter_found = true;
            break;
          }
        }
        if (sub_filter_found) {
          // we have at least one <new_conjunction> subfilters - so can just add
          // the value to each subfilter or create an <new_conjunction> subfilter
          // if we find a single value
          for (i = 1; i < current_value.length; i++) {
            if ($.isArray(current_value[i])) {
              current_value[i].push(value);
            } else {
              current_value[i] = new Array(new_conjunction, current_value[i], value);
            }
          }
        } else {
          // no subfilter - change conjunction, add value and
          // add former filter as <new_conjunction> filter value
          var former = current_value;
          current_value[0] = new_conjunction;
          current_value.push(value);
          current_value.push(former);
        }
      }
      return current_value;
    }
  };
})(jQuery);