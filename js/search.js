/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
  Drupal.behaviors.drupli_search = {
    attach: function (context) {
      $('#keyword-search-textfield').keypress(function(event) {
        if ( event.which == 13 ) {
          Drupal.behaviors.drupli_search.startSearch();
          return false;
        }
      });
    },
    startSearch: function () {
      var base = location.protocol + "//" + location.host + Drupal.settings.basePath;
      $url = base + Drupal.settings.drupli_search.path + '/';
      // build filter url:
      for(var key in Drupal.settings.drupli_search.filter) {
        // we can just use
        $filter = Drupal.settings.drupli_search.filter[key];
        // check mode
        if($filter.seo_optimized_active) {
          $url = $url + $filter.seo_optimized_key + $filter.seo_optimized_key_value_seperator;
        }
        // check if we have fulltext filter - otherwise use exception value
        if (key == 'search_api_views_fulltext') {
          var val = $('#keyword-search-textfield').attr('value');
          // check the user entry
          if (val == '') {
            $url = $url + $filter.exception + '/';
          } else {
            val = Drupal.behaviors.drupli_search.parseFulltextField(val);
            $url = $url + val + '/';
          }
          // ignore the remaining filters
          // @TODO: is this wise?
          break;
        } else {
          $url = $url + $filter.exception + '/';
        }
      }
      // redirect
      window.location = $url;
    },
    parseFulltextField: function (value) {
      // check for quotes
      if (value.indexOf('"') != -1) {
        var temp = value.split('"');
        var val = '';
        var quoted = true;
        for (i = 0; i < temp.length; i++) {
          if (temp[i] == '') {
            continue;
          } else {
            if (quoted) {
              val += temp[i];
            } else {
              val += Drupal.behaviors.drupli_search.replaceFulltextFieldSeperators(temp[i]);
            }
            quoted = (quoted == false);
          }
        }
        return val;
      } else {
        // otherwise we can blindly insert seperators
        return Drupal.behaviors.drupli_search.replaceFulltextFieldSeperators(value);;
      }
    },
    replaceFulltextFieldSeperators: function (value) {
        value = value.replace(/, /g, ',');
        value = value.replace(/  /g, ' ');
        value = value.replace(/ /g, Drupal.settings.drupli_search.filter.search_api_views_fulltext.seo_optimized_and_value_seperator);
        value = value.replace(/,/g, Drupal.settings.drupli_search.filter.search_api_views_fulltext.seo_optimized_or_value_seperator);
        value = value.replace(/ OR /g, Drupal.settings.drupli_search.filter.search_api_views_fulltext.seo_optimized_sor_value_seperator);
      return value;
    }
  };
})(jQuery);