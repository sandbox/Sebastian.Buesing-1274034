/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
  Drupal.behaviors.drupli_search_block_config = {
    attach: function (context) {
      // override links
      $('a.drupli_search-rb-filter-settings').bind('click', function( event ) {
        $link = $(this);
        $block = '#refine-block-filter-settings-' + $link.attr('alt');
        $block = $($block);
        if($link.html() == Drupal.settings.drupli_search.labels.show_settings) {
          $link.html(Drupal.settings.drupli_search.labels.hide_settings);
          $block.attr('style', 'display:block;');
        } else {
          $link.html(Drupal.settings.drupli_search.labels.show_settings);
          $block.attr('style', 'display:none;');
        }
        return false;
      });
      //
      $('a.drupli_search-rb-filter-status').bind('click', function( event ) {
        $link = $(this);
        $item = '#refine-block-filter-status-' + $link.attr('alt');
        $item = $($item);
        if($item.attr('value') == 0) {
          $link.html(Drupal.settings.drupli_search.labels.status_disable);
          $item.attr('value', 1);
        } else {
          $link.html(Drupal.settings.drupli_search.labels.status_enable);
          $item.attr('value', 0);
        }
        return false;
      });
      // table-drag-changed-warning
    }
  };
})(jQuery);